'use strict';

const cheerio = require('cheerio');
const Request = require('./request');

class Category {
  
  constructor(options) {
    this.params = options;
    this.products = [];
  }

  parseCategories() {
    return new Promise((res, rej) => {
      let req = new Request(this.params.url);
      req.get((err, body) => {
        if(err) return rej(err);

        let $ = cheerio.load(body);
        let categories = $('li.item.__submenu:not(.__action)').find('.subcatalog_w a.subcatalog_title');
        let urls = categories.map((i, el) => {
          return this.params.url + el.attribs.href
        }).get()/*.slice(0, 5);*/
        this.urls = urls;

        console.log(`Collecting of main categories from ${this.params.url} was complited. ${urls.length} categories was found. Ok.`);
        res(urls);
      })
    })
  }

  getProductsList(url) {
    let limit = 10000;
    return new Promise((res, rej) => {
      let req = new Request(url + `?limit=${limit}`);

      req.get((err, body) => {
        if(err) return rej(`Unable to parse category ${url}. Error: ${err}`);

        let $ = cheerio.load(body);
        let products = $('a.catalog-i_link');
        if(products.length == 0)
          products = $('.p-product h1 a');

        let links = products.map((i, el) => {
          return el.attribs.href
        }).get()/*.slice(0, 5);*/

        this.products = this.products.concat(links);
        res(`Category parsed, products: ${links.length} ${url}`);
      })
    })
  }

  getProducts() {
    this.len = this.urls.length;
    this.counter = 0;

    return new Promise((res, rej) => {
      this.urls.forEach(url => {
        this.getProductsList(url)
        .then((msg) => {
          console.log(process.pid, msg);
          if(++this.counter >= this.len)
            return res(this.products)
        })
        .catch(e => {
          this.len--;
          console.log(process.pid, e)
        })
      });
    })
    
  }
}

module.exports = Category;