'use strict';

const cheerio = require('cheerio');
const Product = require('./product');
const Request = require('./request');

class Grubber {
  constructor(urls, table, dir) {
    this.table = table;
    this.dir = dir;
    this.counter = 0;
    this.links = urls;
    this.counter = this.links.length;

    // console.log(process.pid, 'Links length:', this.links.length)
  }

  isDone(msg) {
    this.counter--;
    console.log(process.pid, this.counter + ' products left', msg);
    if(this.counter <= 0) 
      process.exit(0);

    this.parseProducts();
  }

  parseProducts() {
    //this.links.forEach(link => {
      let link = this.links.pop();

      let product = new Product(link, this.table, this.dir);
      product.parse()
      .then(msg => this.isDone(msg))
      .catch(err => this.isDone(err))
    //});
  }
}

module.exports = Grubber;