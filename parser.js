'use strict'; 

const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const Settings = require('./settings');
const Category = require('./category');
const Grubber = require('./grubber');

if (cluster.isMaster) {
  let start = new Date();

  let settings = new Settings(process.argv.slice(2));
  let category = new Category(settings.params);
  let messageToProcess = {};
  let productsCount = 0, numberOfProcesses = numCPUs;

  settings.createDirectory()
  .then(err => {
    return settings.removeFile()
  })
  .then(dir => {
    messageToProcess.dir = dir;
    return settings.parseXLS()
  })
  .then(table => {
    messageToProcess.table = table;
    return category.parseCategories();
  })
  .then(urls => {
    if(urls.length == 0) {
      console.log(`There no categories found at first. Exit.`);
      process.exit(0);
    }

    return category.getProducts();
  })
  .then(urls => {
    console.log(`${category.products.length} products found.`);
    let chunk = category.products.length / numCPUs;
    for (var i = 0; i < numCPUs; i++) {
      messageToProcess.urls = category.products.slice(i * chunk, i * chunk + chunk);
      let worker = cluster.fork();
      worker.send(messageToProcess);
    }
  })
  .catch(e => {
    console.log('Error:', e.stack);
    process.exit(0);
  })

  cluster.on('exit', (worker, code, signal) => {
    console.log(`Worker ${worker.process.pid} has finished his job.`);
    let end = (new Date() - start) / 1000;
    let minutes = Math.floor(end / 60);
    let seconds = end - minutes * 60;
    console.info(`Parsing time: ${minutes} min ${seconds} sec`)

    numberOfProcesses--;
    if(numberOfProcesses <= 0) {
      console.log(`Parsing was completed. ${category.products.length} products has been found`);

      process.exit(0);
    }
  });
} else {
  process.once('message', msg => {
    if(msg.urls.length == 0) {
      console.log(`There no categories found. Exit.`);
      process.exit(0);
    }

    console.log(process.pid, `Start to collect products from categories.`);
    let parser = new Grubber(msg.urls, msg.table, msg.dir);
    parser.parseProducts();
  })
}



