'use strict';

const cheerio = require('cheerio');
const Request = require('./request');
const fs = require('fs');
const request = require('request');

let Table = {}, stream = null;

class Product {
  constructor(url, table, dir) {
    this.url = url;
    Table = table;
    this.product = new Map();
    this.dir = dir;
    if(stream == null)
      stream = fs.createWriteStream(this.dir + '/output.csv', {flags: 'a'});
  }

  parse() {
    return new Promise((res, rej) => {
      let req = new Request(this.url);

      req.get((err, body) => {
        if(err) {
          return rej(`Unable to parse product ${this.url}, Error: ${err}`);
        }

        this.container = cheerio.load(body);
        this.getArticul();

        if(Table[this.product.get('articul')] != undefined) {
          this.getName();
          for (let key in Table[this.product.get('articul')])
            this.product.set(key, Table[this.product.get('articul')][key]);
        
          this.getDescription();
          this.getPhoto();

          stream.write(this.getRow('\t'), 'utf8', (e, stat) => {
            if(e)
              return rej(`Unable to save product ${this.url}, Error: ${e}`);

            let saveImage = request(this.src).pipe(fs.createWriteStream(this.product.get('photo')));
            saveImage.on('finish', () => {
              res(`${this.product.get('name')} was saved to file and the photo downloaded`);
            })
            .on('error', e => {
              rej(`${this.product.get('name')} was saved but there was download error, ${e}`);
            })
          })
        } else {
          res(`There no such product at Table. ${this.product.get('articul')}. ${this.url}`);
        }
      })
    })
  }

  getRow(separator) {
    let vals = [];
    for(let k of this.product.values())
      vals.push(k.toString().replace(/(\r\n|\n|\r)/gm, "").replace(/(\t)/gm," ").trim());
    return vals.join(separator) + '\n';
  }

  getArticul() {
    let articul = this.container('.b-product-main__infoline span span').text();
    if(articul == '') {
      // let s = fs.createWriteStream(process.pid + '.log', {flags: 'w'});
      // s.write(body);
      articul = this.container('.p-product .article strong').text().split(' ').pop();
    }
    this.product.set('articul', articul);
  }

  getName() {
    let name = this.container('.b-product-main__top h1').text();
    this.product.set('name', name);
  }

  getPhoto() {
    let src = this.container('.b-product-main__data-gallery-big > img').eq(0);
    if(src.length == 0) 
      src = this.container('.images .swiper-slide img').eq(0);

    src = src.attr('src');
    this.src = src;
    let photo = [this.dir, this.product.get('articul')].join('/') + '.jpg';
    this.product.set('photo', photo);
  }

  getDescription() {
    let description = this.container('.b-product-main__info .b-product-main__info-descr').text();
    this.product.set('description', description);
  }
}

module.exports = Product;