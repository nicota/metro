'use strict';

const request = require('request');
const userAgent = require('random-useragent');
const fs = require('fs');

class Request {
  constructor(url) {
    this.url = url;
  }

  get(cb, times) {
    times = times || 0;
    this.request()
    .then(res => {
      cb(null, res);
    })
    .catch(err => {
      console.log(process.pid, `wait for ${this.url}`);
      times++;
      if(times >= 3) return cb(err)

      console.log(`Can't get ${this.url}. Error: ${err}. Trying againg. ${times} times`);
      this.get(cb, times);
    })
  }

  request() {
    return new Promise((res, rej) => {
      let ua = userAgent.getRandom();

      request.get({
        url: this.url,
        rejectUnauthorized: false,
        headers: {
          'User-Agent': ua
        }
      }, (error, response, body) => {
        if (!error && response.statusCode == 200)
          return res(body);

        return rej(error);
      })
    })
  }

}

module.exports = Request;