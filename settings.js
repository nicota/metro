'use strict';

const xlsx = require('node-xlsx').default;
const cheerio = require('cheerio');
const Request = require('./request');
const fs = require('fs');

class Settings {
  
  constructor(argv) {
    this.params = {};
    argv.forEach(option => {
      let parts = option.split('=');
      this.params[parts[0]] = parts[1];
    });
    this.folder = 'output/' + this.params.url.split('/').pop();
  }

  createDirectory() {
    return new Promise((res, rej) => {
      fs.stat(this.folder, (err, stat) => {
        if(err) {
          fs.mkdir(this.folder, err => {
            if(err) return rej(`Unable to create folder ${this.folder}`, err);
            console.log(`${this.folder} directory was created. Ok.`);
            res(this.folder);
          });
        } else {
          console.log(`${this.folder} directory was created before paser was started. Ok.`);
          res(this.folder);
        }
      })
    });
  }

  removeFile() {
    return new Promise((res, rej) => {
      let file = this.folder + '/output.csv'; 
      fs.unlink(file, err => {
        console.log(`Output file ${file} was removed. Ok.`);
        res(this.folder);
      });
    });
  }

  parseXLS() {
    return new Promise((res, rej) => {
      const table = xlsx.parse(this.params.file)[0];

      let data = {};
      table.data.forEach(row => {
        data[row[9]] = {
          articul: row[9],
          name: row[10],
          price: parseFloat(row[14]).toFixed(2)
        }
      });

      console.log(`Input ${this.params.file} file was loaded. ${table.data.length} items found. Ok.`)
      res(data)
    })
  }
}

module.exports = Settings;